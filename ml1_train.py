from cProfile import label
import torch
import torchvision.models as models
from sklearn.datasets import load_iris
import matplotlib.pyplot as plt
from pathlib import Path
from torchvision import datasets
from torchvision.transforms import ToTensor
from torch.utils.data import DataLoader
from torch import optim
from torch import nn
import torch.nn.functional as F
from torchvision import transforms


# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(device)


#Launch tensorboard 
from torch.utils.tensorboard import SummaryWriter # TensorBoard support
tb = SummaryWriter(comment='Run FastRCNN')


#Download train and test fiftyone datasets
import fiftyone as fo
import fiftyone.zoo as foz


train_dataset = foz.load_zoo_dataset(
  "voc-2012", 
  split="train",
  max_samples=5000,
  shuffle=True,
)


#Create CustomDatasets from train and test fiftyone datasets
from MyDataset import CustomDataset
from torchvision.transforms.transforms import ToPILImage
trainDataset = CustomDataset(train_dataset,
                             transform = transforms.Compose([transforms.ToPILImage(),
                                                             transforms.Resize((256)),
                                                            transforms.ToTensor()])
                              )


#Test image selection, pick and save one sample
id = 5 
print(f" Samples ID: {trainDataset.get_sample_id(id)}")
image, target = trainDataset[id]

from PIL import Image
img = Image.open(train_dataset[trainDataset.get_sample_id(id)]['filepath'])
img.save('test_image.png')


#Function to stack our samples into batch
def collate_fn(batch):
    images,labels = zip(*batch)
    images = [x.to(device) for x in images]
    labels = [{"boxes":x["boxes"].to(device),"labels":x["labels"].to(device)} for x in labels]
    return images,labels

#Create train and test DataLoaders
train_loader =  torch.utils.data.DataLoader(trainDataset, 
                                          batch_size=4, 
                                          shuffle=True, 
                                          collate_fn = collate_fn)


import torchvision
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor

model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True)

num_classes = 21  # 20 class  + background
in_features = model.roi_heads.box_predictor.cls_score.in_features

model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)
model = model.to(device)

params = [p for p in model.parameters() if p.requires_grad]
optimizer = torch.optim.SGD(params, lr=0.005,
                            momentum=0.9, weight_decay=0.0005)

lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer,
                                                step_size=3,
                                                gamma=0.1)



torch.cuda.empty_cache()

from vision.references.detection.engine import train_one_epoch
num_epochs = 10

for epoch in range(num_epochs):
  train_one_epoch(model, optimizer, train_loader, device, epoch, 50,tb)
  lr_scheduler.step()

torch.save(model,'model_5000.pt')

