from torch.utils.data import Dataset
from torchvision.io import read_image
import torch 

class CustomDataset(Dataset):
    def __init__(self, fiftyone_dataset, transform=None):
        self.image_size = 256
        self.fiftyone_dataset = fiftyone_dataset
        self.transform = transform
        self.id_list = [sample.id for sample in self.fiftyone_dataset]
        self.labels = {
                        "person":1,
                        "bird":2,
                        "cat":3,
                        "cow":4,
                        "dog":5,
                        "horse":6,
                        "sheep":7,
                        "aeroplane":8,
                        "bicycle":9,
                        "boat":10,
                        "bus":11,
                        "car":12,
                        "motorbike":13,
                        "train":14,
                        "bottle":15,
                        "chair":16,
                        "diningtable":17,
                        "pottedplant":18, 
                        "sofa":19,
                        "tvmonitor":20
                      }


    def __len__(self):
        return len(self.id_list)


    def get_sample_id(self,idx:int):
      return self.id_list[idx]


    def __getitem__(self, idx):
        sample = self.fiftyone_dataset[self.id_list[idx]]
        img_path = sample.filepath
        image = read_image(img_path)

        boxes = []
        labels = []
        for detection in sample.ground_truth.detections:
          x_top_left = detection.bounding_box[0]*self.image_size
          y_top_left = detection.bounding_box[1]*self.image_size

          width = detection.bounding_box[2]*self.image_size
          height = detection.bounding_box[3]*self.image_size

          x_bottom_right = x_top_left + width
          y_bottom_right = y_top_left + height

          boxes.append([x_top_left,y_top_left,x_bottom_right,y_bottom_right,])
          labels.append(self.labels[detection.label])

        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels = torch.tensor(labels,dtype=torch.int64)

        target = {
                  "boxes":boxes,
                  "labels":labels
                  }
        if self.transform:
            image = self.transform(image)

        return image, target