from cProfile import label
from tkinter import TOP
from importlib_metadata import re
from sklearn.feature_extraction import image
import torch
import torchvision.models as models
from sklearn.datasets import load_iris
import matplotlib.pyplot as plt
from pathlib import Path
from torchvision import datasets
from torchvision.transforms import ToTensor, ToPILImage
from torch.utils.data import DataLoader
from torch import optim
from torch import nn
import torch.nn.functional as F
from torchvision import transforms
import numpy as np


# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(device)


import fiftyone as fo
import fiftyone.zoo as foz
test_dataset = foz.load_zoo_dataset(
  "voc-2012", 
  split="validation",
  max_samples=4000,
  shuffle=True,
)


from MyDataset import CustomDataset
from torchvision.transforms.transforms import ToPILImage
testDataset = CustomDataset(test_dataset,
                             transform = transforms.Compose([transforms.ToPILImage(),
                                                             transforms.Resize((256)),
                                                            transforms.ToTensor()])
                              )


#Function to stack our samples into batch
def collate_fn(batch):
    images,labels = zip(*batch)
    images = [x.to(device) for x in images]
    labels = [{"boxes":x["boxes"].to(device),"labels":x["labels"].to(device)} for x in labels]
    return images,labels


#Create train and test DataLoaders
test_loader =  torch.utils.data.DataLoader(testDataset, 
                                          batch_size=4, 
                                          shuffle=True, 
                                          collate_fn = collate_fn)


model = torch.load('model_5000.pt')


torch.cuda.empty_cache()
model.eval()
precisions = []


from mean_avg_precision import mean_average_precision
with torch.no_grad():    
  for images, labels in test_loader:
    results = model(images)
    for result, target in zip(results,labels):
      target_boxes = target['boxes'].cpu().numpy()
      target_labels = target['labels'].cpu().numpy()

      result_boxes = result['boxes']
      result_score = result['scores']
      result_labels = result['labels']

      good_scores = result_score > 0.8
  
      good_result_labels = result_labels[good_scores].cpu().numpy()
      good_result_boxes = result_boxes[good_scores].cpu().numpy()

      target_bboxes = np.array([np.concatenate(([0,int(a),1],b)) for a,b in zip(target_labels,target_boxes)])
      prediction_bboxes = np.array([np.concatenate(([0,int(a),score],b)) for a,b,score in zip(good_result_labels,good_result_boxes,result_score[good_scores].cpu().numpy())])
      res = mean_average_precision(prediction_bboxes,target_bboxes)
      precisions.append(res.item())

  print(np.mean(precisions))    



